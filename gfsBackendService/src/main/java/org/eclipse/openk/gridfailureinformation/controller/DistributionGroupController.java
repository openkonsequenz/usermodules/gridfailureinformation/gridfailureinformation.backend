/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.ConflictException;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/distribution-groups")
public class DistributionGroupController {
    private final DistributionGroupService distributionGroupService;

    public DistributionGroupController(DistributionGroupService distributionGroupService) {
        this.distributionGroupService = distributionGroupService;
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen aller Verteilergruppen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<DistributionGroupDto> findDistributionGroups() {
        return distributionGroupService.getDistributionGroups();
    }

    @GetMapping("/{uuid}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen einer bestimmten Verteilergruppe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Verteilergruppe wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public DistributionGroupDto readDistributionGroup(
            @PathVariable UUID uuid) {
        return distributionGroupService.getDistributionGroupByUuid(uuid);
    }

    @PostMapping
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anlegen einer neuen Verteilergruppe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Verteilergruppe erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<DistributionGroupDto> insertDistributionGroup(@Validated @RequestBody DistributionGroupDto distributionGroupDto) {
        DistributionGroupDto savedDistributionGroupDto = distributionGroupService.insertDistributionGroup(distributionGroupDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedDistributionGroupDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedDistributionGroupDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Eine bestimmte Verteilergruppe löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    @DeleteMapping("/{groupUuid}")
    public void deleteDistributionGroup(
            @PathVariable UUID groupUuid) {
        try {
            distributionGroupService.deleteDistributionGroup(groupUuid);
        }
        catch ( Exception e ) {
            log.error("Exception in delete distribution group: ", e);
            throw new ConflictException("distribution.group.still.in.use");
        }
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Eine bestimmte Verteilergruppe bearbeiten.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Verteilergruppe nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Verteilergruppe erfolgreich geändert.")})
    @PutMapping("/{groupUuid}")
    public ResponseEntity<Void> updateDistributionGroup(
            @PathVariable UUID groupUuid,
            @Validated @RequestBody DistributionGroupDto distributionGroupDto) {
        if (!groupUuid.equals(distributionGroupDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        distributionGroupService.updateGroup(distributionGroupDto);
        return ResponseEntity.ok().build();
    }
}
